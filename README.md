# chef_docker_swarm

This repo contains A docker-swarm-compose.yml which creates a stack containing 1 chef server 1 chef workstation and N chef nodes. Each chef node executes a run_list defined in the docker-swarm-compose.yml file (as an env variable for the containers) for the service the node corresponds to.

The chef workstation service waits for the chef server to be running and then uploads a cookbook of recipes contained within the workstation image. The chef node(s) wait until the workstation service is running and then ssh to it and run the proper commands to bootstrap themselves as chef=managed nodes, set their run_list to the run_list configured in the docker-swarm-compose.yml file, and execute 'chef-client'.

The docker-swarm-compose.yml file can be edited to contain any number of services which all use the node_image docker image. Each of these services should specify their own run_list env. When these services start up they will bootstrap themselves as chef worker nodes and execute the provided chef run_list.

This approach allows you to specify N custom services via chef recipe composition while having them managed by docker swarm. This is opposed to having to construct N docker images, one for each service, ahead of time. Chef is used to configure each chef_node so that the service running on that node can function properly while using the same core base image.

Consequently there is no need to manipulate docker files to create or manage a new service for your stack. Instead all that is needed is the creation//modification of chef recepies used within the workstation_image and then updating the docker-swarm-compose.yml file to add your new service.

(example run command to create the chef-managed docker stack:)

      docker swarm init; docker stack deploy my_new_stack --compose-file=./docker-swarm-compose.yml

(example service added to docker-swarm-compose.yml:)

      enigma:
        image: node_image
        deploy:
          replicas: 2
        healthcheck:
          test: ["CMD-SHELL", "which chef-client"]
          interval: 30s
          timeout: 10s
          retries: 30
          start_period: 3m
        environment: 
          - run_list=recipe[example::install_java],recipe[example::start_enigma]
        ports:
          - "8093:8080"
        command: ["wait-for-workstation.sh"]
        volumes:
          - chef_pem:/root/chef-repo/.chef/


(exmaple chef recipe added to workstation_image and leveraged by the above enigma service:)

      bash 'start enigma' do
          code <<-EOH
          pkill -f 'java.*enigma' || true
          rm /enigma.jar || true
          curl -g -L 'http://192.168.65.2:8082/service/rest/v1/search/assets/download?repository=maven-snapshots&group=com.enigma&name=enigma&sort=version&direction=desc' > enigma.jar
          java -jar /enigma.jar --server.port=8080 &
          EOH
      end