#!/bin/sh

until sshpass -p '123456' ssh -o StrictHostKeyChecking=no workstationUser@chef_workstation "exit"; do
  >&2 echo "Workstation is unavailable - sleeping"
  sleep 90
done

run_chef_node.sh