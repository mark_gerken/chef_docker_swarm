#!/bin/sh

set -x

/usr/sbin/sshd -D &

# bootstrap the client via the workstation
sshpass -p '123456' ssh -o StrictHostKeyChecking=no workstationUser@chef_workstation "cd ~/chef-repo/ && knife bootstrap $(hostname -I | awk '{print $NF}') -x nodeUser -P 123456 --node-ssl-verify-mode none --sudo --node-name $(hostname)"
# set the run_list of the chef-client-node
sshpass -p '123456' ssh -o StrictHostKeyChecking=no workstationUser@chef_workstation "cd ~/chef-repo/ && knife node run_list set $(hostname) '$run_list'"

# run the local chef client
chef-client

tail -f /dev/null