#!/bin/sh

set -x

/opt/opscode/embedded/bin/runsvdir-start &
chef-server-ctl reconfigure

mkdir ~/.chef
chef-server-ctl user-create chefadmin chef admin chefadmin@gmail.com '123456' --filename ~/.chef/chefadmin.pem
chef-server-ctl org-create chef-on-ubuntu "Chef Infrastructure on Ubuntu 14.04" --association_user chefadmin --filename ~/.chef/chef-on-ubuntu.pem

tail -F /opt/opscode/embedded/service/*/log/current
