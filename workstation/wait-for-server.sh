#!/bin/sh

cp /tmp/config.rb /home/workstationUser/chef-repo/.chef/config.rb
cd /home/workstationUser/chef-repo
until knife client list; do
  >&2 echo "Server is unavailable - sleeping"
  sleep 90
done

run_chef_workstation.sh