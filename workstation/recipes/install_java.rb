bash 'install java' do
    code <<-EOH
      apt-get update
      apt-get install default-jre -y
      apt-get install curl -y
    EOH
    not_if {"`which java`" == '/usr/bin/java'}
  end