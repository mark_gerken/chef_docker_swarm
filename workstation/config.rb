current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                'chefadmin'
client_key               "chefadmin.pem"
validation_client_name   'chef-on-ubuntu-validator'
validation_key           "chef-on-ubuntu-validator.pem"
chef_server_url          'https://chef_server/organizations/chef-on-ubuntu'
ssl_verify_mode :verify_none
cache_type               'BasicFile'
cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
cookbook_path            ["#{current_dir}/../cookbooks"]
knife[:editor]="/usr/bin/vi"