#!/bin/sh

set -x
cp /tmp/config.rb /home/workstationUser/chef-repo/.chef/config.rb

# upload the example cookbook to the configured chef_server
cd /home/workstationUser/chef-repo/ && knife cookbook upload example
sudo /usr/sbin/sshd -D